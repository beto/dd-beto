[English](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_en.md) - [Català](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README.md)

### License

AGPLv3 (https://www.gnu.org/licenses/agpl-3.0.en.html)

### Credits

Pilot project of the Democratic Digitalization Plan led by Xnet and promoter families. Software created by IsardVDI and 3iPunt with the collaboration of MaadiX.net, Affac, the Barcelona City Hall and the Barcelona Education Consortium

# What's this

This project allows to bring up with ease a full identity provider and many apps to have an environment thought for schools and universities. The project will provide an integrated solution to handle the common environment in education:

- **Classrooms**: A moodle instance with custom theme and custom plugins.
- **Cloud drive**: A nextcloud instance with custom theme and custom plugins.
- **Documents**: An onlyoffice instance integrated with nextcloud.
- **Web pages**: A wordpress instance with custom theme and custom plugins.
- **Pad**: An etherpad instance integrated with nextcloud.
- **Conferences**: A BigBlueButton integrated with moodle and nextcloud. This needs an standalone host right now 
- **Forms**: A forms nextcloud plugins.
- ... (some apps are there like jitsi or BigBlueButton but not fully integrated right now)

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

# Administration interface

Now there is an administration interface that allows to easily manager users and groups and keep it in sync between applications. This is done by executing actions on the applications apis.

| ![](docs/img/admin_sync.png) | ![](docs/img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

To easily migrate and populate system there are also two provided imports for users and groups:

- From Google Admin Console as a JSON dump
- From a CSV file

This admin interface is now in an alpha status but allows to manage users in sync between keycloak, moodle and nextcloud.

# Project status

It works but as we are working on it it will have lots of improvements upcoming months. Some automatizations need to be done, specially with SAML integration in moodle and Keycloak initial personalization.

Your collaboration is wellcome! Just fork this repo and do a PR or open us an issue.

# DigitalDemocratic Documentation

This documentation is writed in Markdown using [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs).

See `docs` directory for Markdown files or the [auto-built site](https://digitaldemocratic.gitlab.io/digitaldemocratic).

## Quick start

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Change default passwords

```
./securize_conf.sh
```

Edit digitaldemocratic.conf file variables to suit your needs.

```
cp -R custom.sample custom
```

Edit and replace files to personalize system.

The first time execute:
```
./dd-ctl update-repo
```
And then:
```
./dd-ctl all
```

NOTE: The SAML Auth in plugin automation status now is:
- Moodle: Not fully automated.
    1. Login to moodle as admin via: https://moodle.\<domain\>/login/index.php?saml=off
    2. Go to authentication configuration: https://moodle.\<domain\>/admin/settings.php?section=manageauths
    3. Enable SAML2 clicking the eye.
    4. Clic on *configuration* on SAML2
    5. Click on the *Regenerate certificate* button inside the form. After that go back to SAML2 configuration page.
    6. Click on the *Lock certificate* button.
    7. In the terminal execute the script to autoconfigure: docker exec isard-sso-admin python3 moodle_saml.py
    8. The last thing is to purge moodle cache: [[missing docker exec php-fpm7 script, do it through moodle web ui]]

- Nextcloud: Fully automated. After finishing the *make all* should be ready. In case it fails refer to isard-sso/docs.
- Wordpress: Fully automated. After finishing the *make all* should be ready. In case it fails refer to isard-sso/docs.

## Extended

You can start this project in any docker & docker-compose host (any OS should work). To install those packages in your distro refer to docker & docker-compose in this documentation and in the sysadm folder you have some scripts.

Any distro should work but, if you want to use our sysadm scripts to install docker & docker compose use Debian Buster (10).

### Clone and submodules

```
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive
```

### docker

Refer to the official documentation (https://docs.docker.com/engine/install/) or use our sysadm script if you are on a Debian Buster (10).

### docker-compose

Refer to the official documentation (https://docs.docker.com/compose/install/) or use our sysadm script if you are on a Debian Buster (10).

### Setup

Copy digitaldemocratic.conf.sample to digitaldemocratic.conf and edit to satisfy your needs. At least (to development) you should adapt
the DOMINI envvar to your root domain.

- PRODUCTION: You'll need a multidomain dns (or redirect all the subdomains) to your server IP.
- DEVELOPMENT: You'll have to edit your /etc/hosts and add all the required domains to your development server IP.

#### Subdomains
- Keycloak: sso.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>
- (optional) FreeIPA: ipa.<yourdomain.org>

### Customization

Recursively copy the *custom.sampl* folder to *custom* and edit the yaml personalization files and substitute the image.

### Start the project

The first time (and to upgrade later) you should execute:
```
./dd-ctl update-repo
```
And after:
```
./dd-ctl all
```

Then you can control the up/down:
```
./dd-ctl down
./dd-ctl up
```

### Integration

Read the [SAML_README.md](https://gitlab.com/isard/isard-sso/-/blob/master/docs/SAML_README.md) in isard-sso/docs folder
to known more about setting and customizing applications, specially if the SSO fails to get correctly configured the
first time.

